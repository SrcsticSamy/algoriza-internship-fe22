import axios from 'axios';
import { useUserStore } from '../stores/UserStore';

const API = axios.create({
  baseURL: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/',
  headers: {
    // !: move API key to .env file
    'X-RapidAPI-Key': 'b8904dec21mshace7b65a8dcba4dp1530fdjsnaa7a1c7bd036',
    'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com',
  },
});

API.interceptors.request.use(
  (config) => {
    // must be within the interceptor or it will cause an error
    // because the store is not yet initialized
    const userStore = useUserStore();

    if (!userStore.isLoggedIn) {
      throw new Error('Not logged in');
    } else if (userStore.isTokenExpired) {
      throw new Error('Token expired. Log out and log back in.');
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

// API Endpoints call functions
export const searchDestinations = (query) => {
  return API.get('searchDestination', {
    params: {
      query,
    },
  });
};

export const searchHotels = (query) => {
  return API.get('searchHotels', {
    params: {
      search_type: 'CITY',
      languagecode: 'en-us',
      currency_code: 'USD',
      ...query,
    },
  });
};

export const getHotelDescription = (hotelId) => {
  return API.get('getDescriptionAndInfo', {
    params: {
      hotel_id: hotelId,
      languagecode: 'en-us',
    },
  });
};

export const getSortByOptions = (query) => {
  return API.get('getSortBy', {
    params: {
      search_type: 'CITY',
      ...query,
    },
  });
};

export const getHotelDetails = (hotel_id, arrival_date, departure_date) => {
  return API.get('getHotelDetails', {
    params: {
      hotel_id,
      arrival_date,
      departure_date,
      languagecode: 'en-us',
      currency_code: 'USD',
    },
  });
};
