export const getDayAfterDate = (date) => {
  const dayAfterDate = new Date(date);
  dayAfterDate.setDate(dayAfterDate.getDate() + 1);
  return dayAfterDate;
};

export const formatDate = (date) => {
  // get date as Mar 18, 2021
  const options = { year: 'numeric', month: 'short', day: 'numeric' };
  return new Date(date).toLocaleDateString('en-US', options);
};

export const generateTokenExpDate = (currentDate) => {
  // should expire in 1h
  // currentDate is in milliseconds and functions returns date in milliseconds
  const hour = 60 * 60 * 1000;
  return currentDate + hour;
};

export const calculateNumberOfDays = (startDate, endDate) => {
  const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
  return Math.round(Math.abs((startDate - endDate) / oneDay));
};
