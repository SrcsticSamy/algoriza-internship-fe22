export const toFiveStars = (score) => {
  if (score === 0) return 0;
  if (score >= 9) return 5;

  // Divide the score by 2 to convert it to a 5-point scale
  let tempScore = score / 2;

  // Round the score to the nearest 0.5
  return Math.round(tempScore * 2) / 2;
};

export const calculateSalePercentage = (price, salePrice) => {
  const difference = price - salePrice;
  const percentage = (difference / price) * 100;
  return Math.round(percentage);
};
