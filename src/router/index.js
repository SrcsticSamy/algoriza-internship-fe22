import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Auth from '../views/Auth.vue';
import Hotels from '../views/Hotels.vue';
import HotelDetails from '../views/HotelDetails.vue';
import Checkout from '../views/Checkout.vue';
import Reservations from '../views/Reservations.vue';
import AppNavbar from '../components/AppNavbar.vue';
import AppFooter from '../components/AppFooter.vue';
import { useUserStore } from '../stores/UserStore';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: Home,
        Navbar: AppNavbar,
        Footer: AppFooter,
      },
    },
    {
      path: '/auth',
      name: 'auth',
      components: {
        default: Auth,
        Navbar: AppNavbar,
      },
    },
    {
      path: '/hotels',
      name: 'hotels',
      components: {
        default: Hotels,
        Navbar: AppNavbar,
        Footer: AppFooter,
      },
    },
    {
      path: '/hotels/:id',
      name: 'hotel-details',
      props: true,
      components: {
        default: HotelDetails,
        Navbar: AppNavbar,
        Footer: AppFooter,
      },
    },
    {
      path: '/checkout',
      name: 'checkout',
      components: {
        default: Checkout,
        Navbar: AppNavbar,
      },
    },
    {
      path: '/reservations',
      name: 'reservations',
      components: {
        default: Reservations,
        Navbar: AppNavbar,
        Footer: AppFooter,
      },
    },
  ],
  scrollBehavior() {
    return {
      top: 0,
      behavior: 'smooth',
    };
  },
});

router.beforeEach((to, from, next) => {
  const userStore = useUserStore();
  const publicPages = ['/', '/auth'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = userStore.isLoggedIn;

  if (authRequired && !loggedIn) {
    return next('/auth');
  }

  if (loggedIn && to.path === '/auth') {
    return next('/');
  }

  next();
});

export default router;
