import { defineStore } from 'pinia';

export const useReservationsStore = defineStore('reservations', {
  state: () => ({
    savedReservations: [],
    checkoutReservation: null,
  }),

  getters: {},

  actions: {
    saveReservation() {
      this.savedReservations.push(this.checkoutReservation);
    },

    setCheckoutReservation(reservation) {
      this.checkoutReservation = reservation;
    },

    clearCheckoutReservation() {
      this.checkoutReservation = null;
    },
  },

  persist: true,
});
