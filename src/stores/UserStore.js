import { defineStore } from 'pinia';
import { decodeToken, generateToken } from '../utils/TokenHandlers';
import { generateTokenExpDate } from '../utils/DateHelpers';

export const useUserStore = defineStore('user', {
  state: () => ({
    token: null,
  }),

  getters: {
    isLoggedIn: (state) => !!state.token,

    isTokenExpired: (state) => {
      if (!state.token) return false;
      const { exp } = decodeToken(state.token);
      return Date.now() > exp;
    },
  },

  actions: {
    login(email) {
      const token = generateToken({
        email,
        iss: Date.now(),
        exp: generateTokenExpDate(Date.now()),
      });
      this.token = token;
      return token;
    },

    logout() {
      this.token = null;
    },
  },

  persist: true,
});
