/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        'work-sans': ['Work Sans', 'sans-serif']
      },
      colors: {
        primary: '#2F80ED',
        faded: '#4F4F4F',
        'primary-text': '#181818',
        'secondary-text': '#333333',
        input: '#F2F2F2'
      },
      backgroundImage: {
        hero: "url('/src/assets/images/hero.webp')",
        'mobile-banner': "url('/src/assets/images/mobile-banner.webp')",
        'gradient-hero': 'linear-gradient(180deg, #2969BF 0%, #144E9D 100%)',
        'gradient-promo': 'linear-gradient(180deg, #4796FF 0%, #2366BF 100%)'
      }
    }
  },
  plugins: [require('@tailwindcss/forms')]
}
