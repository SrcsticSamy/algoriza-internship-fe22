# My Dream Place [(See It Live!)](https://tripr.vercel.app/)
A website to search and plan for your next trip.


# Libraries/Frameworks Used
- Vue 3
- Tailwind CSS
- Vite
- Vue Router
- Pinia
- Headless UI


# Notes 

I wanted to clean up the code a bit more, but I reach the API's rate limit of 50 requests *really* quick and I can't keep making new accounts every few minutes.

 What's left was:
- [ ] Better HTML overall
- [x] Use built-in Tailwind classes instead of stuff like w-[100px]
- [ ] Seperate the API calls into composables (useSearchResults, useHotelDetails, etc.)
- [ ] Add validation for all inputs 
  - [x]  Auth page
  - [ ]  Checkout page
- [ ] Add a loading state for all API calls
- [ ] Add a 404 page
- [ ] Add toast notifications


## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Format with Prettier

```sh
npm run format
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
